import React, { useEffect, useState } from 'react';
import { MapContainer, TileLayer, Polyline, Marker, Popup, useMapEvents, Rectangle } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import markerIconPng from "leaflet/dist/images/marker-icon.png"
import {Icon} from 'leaflet'
import pointsTxt from '../geojson/middle_points.csv';
import middlePoints from '../geojson/middle_point_ids.csv';
import './Map.css'
import { Paper, Stack } from '@mui/material';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const argFact = (compareFn) => (array) => array.map((el, idx) => [el, idx]).reduce(compareFn)[1]
const argMin = argFact((max, el) => (el[0] < max[0] ? el : max))

// funkcja importuje wszystkie pliki z podanego katalogu
function importAll(r) {
  let heatMaps = {};
  r.keys().map(item => { heatMaps[item.replace('./', '')] = r(item); });
  return heatMaps;
}

const heatMaps = importAll(require.context('../data/CZASYOUT', false, /\.csv/));
const routeTrace = importAll(require.context('../data/TRASYOUT', false, /\.csv/));
let dlon = 0.648 / 2;
let dlat = 0.324 / 2;

let arg_to_id = []
fetch(middlePoints)
  .then(r => r.text())
  .then(text => {
    let array = text.split(/\r?\n/).map((str) => {
        return Number(str)
    });
    arg_to_id = array
  })

// konwersja z float na hex
function ColorToHex(color) {
  var hexadecimal = color.toString(16);
  return hexadecimal.length == 1 ? "0" + hexadecimal : hexadecimal;
}

// konwersja z rgb(float, float float) na string "#000000"
function ConvertRGBtoHex(red, green, blue) {
  return "#" + ColorToHex(red) + ColorToHex(green) + ColorToHex(blue);
}

let points = [];
fetch(pointsTxt)
    .then(r => r.text())
    .then(text => {
        let array = text.split(/\r?\n/).map((str) => {
            return str.split(",").map(Number)
        });
        array.length--;
        points = array;
    });

// rysuje mape
function Map() {
  const position = [51.919229949239835, 12.095430653932153]
  const [start, setStart] = useState(position);
  const [rects, setRects] = useState({"rects": [], "colors": [], "time": []})
  const [time, setTime] = useState(0)
  const [place, setPlace] = useState('');
  const [menu, setMenu] = useState('inline')
  const [deptime, setDeptime] = useState('11-16T08')
  const [hoverId, setHoverId] = useState(0)
  const [currentId, setCurrentId] = useState(0)
  const [polyline, setPolyline] = useState([])

  const handleChange = (event) => {
    setDeptime(event.target.value);
  };

  // dodaje listenery na myszkę. Wykrywa kliknięcia i ruch żeby zmienić punkt startowy i końcowy
  const ShowRects = () => {
    useMapEvents({
      click: (value) => {
        setStart([value.latlng.lat, value.latlng.lng]);
      },
      mousemove: (value) => {
        let distance = points.map((point) => {
          return Math.hypot(point[0] - value.latlng.lat, point[1] - value.latlng.lng)
        })
        let arg = argMin(distance);
        if (arg <= rects.time.length){
          setTime(rects.time[arg])
          if (hoverId != arg)
            setHoverId(arg)
        }
      }
    })

  }

  // rysuje ścieżkę przy zmianie punktu docelowego lub startowego
  useEffect(() => {

    fetch(routeTrace[`${currentId}_2022-${deptime}.csv`])
      .then(r => r.text())
      .then(text => {
        let txt = text.split("\n")
        txt = txt[hoverId].split(",").map(Number)
        txt.forEach((value, index, arr) => {
          arr[index] = [rects.rects[value][0][0] + dlat, rects.rects[value][0][1] + dlon]
        })
        setPolyline(txt);
      })

  }, [hoverId, currentId])

  // dodaje listenera żeby przeunąć div z czasem przy kursorze
  useEffect(() => {
    document.addEventListener('mousemove', function(ev){
      document.getElementById('time').style.top = `${ev.clientY}px`
      document.getElementById('time').style.left =  `${ev.clientX + 15}px`
    },false);

  }, [])

  // wywołuje funkcje zmieniającą dane na mapie przy zmianie punktu startowego
  useEffect(() => {
    calculateRoutes()
  }, [start, deptime])

  const calculateRoutes = () => {
    if(points.length == 0) return

    // znaleźć najbliższy kwadrat
    let distance = points.map((point) => {
      return Math.hypot(point[0] - start[0], point[1] - start[1])
    })

    let arg = argMin(distance);
    setCurrentId(arg)

    setStart(points[arg])

    let data = heatMaps[`${arg_to_id[arg]}_2022-${deptime}.csv`];
    let heat = []
    fetch(data)
      .then(r => r.text())
      .then(text => {
        let array = text.split(/\r?\n/).map((str) => {
          return str.split(",").map(Number)
        });
        array.length--;
        heat = array;


        let tiles = heat.map((element, i) => {
          return [[element[0] - dlat, element[1] - dlon], [element[0] + dlat, element[1] + dlon]]
        })

        let times = heat.map((element) => {
          return element[2]
        })

        const max = Math.max.apply(Math, heat.map(v => v[2]));
        let Colors = heat.map((element, i) => {
          const v = 2 * element[2] / 9588;
          const b = parseInt(Math.max(0, 255 * (1 - 3 * v)))
          const r = parseInt(Math.max(0, 255 * (0.9 * v - 1)))
          const g = 255 - b - r;
          return ConvertRGBtoHex(r, g, b);
        })

        setRects({ "rects": tiles, "colors": Colors, "time": times});
      });
  }


  return (
    <>
    <div id='time'>{Math.floor(time/60)}h {time%60}min</div>
    <MapContainer center={position} zoom={5} scrollWheelZoom={true}>
      <TileLayer
        attribution='Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        url="https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png"
      />
      <Marker position={start} icon={new Icon({ iconUrl: markerIconPng, iconSize: [25, 41], iconAnchor: [12, 41] })} >
        <Popup>
          <a href='https://www.youtube.com/watch?v=cErgMJSgpv0'>zoom in</a>
        </Popup>
      </Marker>
      <ShowRects/>


      {

        rects.rects.map((element, i) => {
          return (<Rectangle key={i} bounds={element} pathOptions={{ color: rects.colors[i], weight: 0, fillOpacity: 0.8 }}>
          </ Rectangle>
        )})

      }
      <Polyline pathOptions={{color: "black"}} positions={polyline} />

    </MapContainer>

    <Paper
        component='form'
        sx={{
            display: { menu },
            p: 1,
            width: 300,
            zIndex: 100,
            position: "absolute",
            top: 20,
            right: 20,
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
        }}>

        <Stack direction="row">
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Departure Time</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={deptime}
              label="Departure Time"
              onChange={handleChange}
            >
              <MenuItem value={'11-16T08'}>16 Listopada  8:00</MenuItem>
              <MenuItem value={'11-16T16'}>16 Listopada 16:00</MenuItem>
              <MenuItem value={'11-19T08'}>19 Listopada  8:00</MenuItem>
              <MenuItem value={'11-19T16'}>19 Listopada 16:00</MenuItem>
            </Select>
          </FormControl>

        </Stack>

    </Paper>
    </>
  )

}

 export default Map;
import React from 'react';
import Map from './Map';

function Home() {

    return (
        <div id='home'>
            <Map />
        </div>
    )
}

 export default Home;
import os
import numpy as np
import pandas as pd
from tqdm import tqdm
from datetime import datetime
from typing import Tuple, Any


def get_schedule_for_stop(
        schedule: pd.DataFrame,
        stop_id: int,
        init_time: np.datetime64 = np.datetime64(datetime.now())
) -> pd.DataFrame:
    """
    Extracts records for given stop, and for a time later than that given in a function argument,
    from transport schedule

    :param schedule: Transport schedule
    :type schedule: pd.DataFrame
    :param stop_id: Stop ID for which records have to be extracted from transport schedule
    :type stop_id: int
    :param init_time: Threshold date and time
    :type init_time: np.datetime64
    :return: Transport schedule with extracted records
    :rtype: pd.DataFrame
    """
    mask = (schedule['stop_id'] == stop_id) & (schedule['departure_time'] >= init_time) & (
        schedule['next_stop_id'].notnull())

    return schedule.loc[mask, :].sort_values('departure_time').drop_duplicates(subset=['next_stop_id'])


def make_graph(
        schedule,
        init_stop,
        init_time: np.datetime64 = np.datetime64(datetime.now()),
        trace_routes: bool = False
) -> Tuple[pd.Series, Any]:
    """
    Runs modified the Dijkstra algorithm to calculate reachability times from initial stop to every stops found in
    schedule. Additionally, it traces routes to all stops (if trace_routes is True)

    :param schedule: Dataframe with transport schedule (departures and arrivals)
    :type schedule: pd.DataFrame
    :param init_stop: ID of the initial stop
    :type init_stop: int
    :param init_time: Date and time for which reachability times has to be calculated
    :type init_time: np.datetime64
    :param trace_routes: Whether to trace routes
    :type trace_routes: bool
    :return: Table with reachability times to every stop, Table with route traces for every stop (if trace_routes is True; otherwise None)
    :rtype: Tuple[pd.Series, Any]
    """

    stop_ids = schedule['stop_id'].unique()
    n_stops = len(stop_ids)

    # Prepare place for results
    unvisited_stops = np.copy(stop_ids)
    visited_stops = np.array([], dtype=np.uint32)
    reach_times = pd.Series(np.full(n_stops, np.iinfo(np.uint16).max), index=stop_ids, dtype=np.uint16)
    reach_times[init_stop] = 0
    previous_stops = pd.Series([[] for _ in range(len(stop_ids))], index=stop_ids)

    for _ in tqdm(range(n_stops)):
        # Get stop with the shortest time
        current_stop = reach_times[unvisited_stops].idxmin()
        current_minutes = reach_times[unvisited_stops].min()
        current_time = init_time + np.timedelta64(current_minutes, 'm')

        # Update visited and unvisited stops
        unvisited_stops = np.setdiff1d(unvisited_stops, np.array([current_stop]))
        visited_stops = np.append(visited_stops, current_stop)

        # Get and merge walking and public transportation schedule for current stop
        current_schedule = get_schedule_for_stop(schedule, current_stop, current_time)
        current_schedule['next_stop_arrival_time'] = current_schedule['next_stop_arrival_time'].apply(
            lambda x: np.timedelta64(x - current_time, 'm').astype(np.uint16))
        current_schedule = pd.Series(current_schedule['next_stop_arrival_time'].values,
                                     index=current_schedule['next_stop_id'].values.astype(np.uint16))

        current_schedule = current_schedule[np.intersect1d(unvisited_stops, current_schedule.index, assume_unique=True)]

        # Update times and previous stops for unvisited stops
        for stop_id, time in current_schedule.iteritems():
            if reach_times[stop_id] > time + current_minutes:
                reach_times[stop_id] = time + current_minutes
                if trace_routes:
                    previous_stops[stop_id] = previous_stops[current_stop][:] + [(current_stop, current_time, time)]

    return (reach_times, previous_stops) if trace_routes else (reach_times, None)


if __name__ == '__main__':
    data_path = '../gtfs_data/FINALNE_DANE'
    results_path = '../gtfs_data/WYNIKI'

    date_columns = ['arrival_time', 'departure_time', 'next_stop_arrival_time']
    schedule = pd.read_csv(os.path.join(data_path, 'schedule_mapped_to_grid.csv'), sep=',', parse_dates=date_columns)

    init_datetimes = [
        np.datetime64(datetime(2022, 11, 16, 8, 0, 0)),
        np.datetime64(datetime(2022, 11, 16, 16, 0, 0)),
        np.datetime64(datetime(2022, 11, 19, 8, 0, 0)),
        np.datetime64(datetime(2022, 11, 19, 16, 0, 0))
    ]

    init_stops = np.sort(schedule['stop_id'].unique())

    for init_datetime in init_datetimes:
        init_datetime_str = np.datetime_as_string(init_datetime, unit='h')

        for init_stop in init_stops:
            times, previous_stops = make_graph(schedule, init_stop, init_datetime, trace_routes=True)

            reach_times = pd.DataFrame({'middle_point_id': times.index, 'reach_time_minutes': times.values})
            route_trace = pd.DataFrame({'middle_point_id': previous_stops.index, 'route_trace': previous_stops.values})

            file_name = f'{init_stop}_{init_datetime_str}.csv'

            reach_times.to_csv(os.path.join(results_path, f'CZASY/{file_name}'), sep=',', index=False)
            route_trace.to_csv(os.path.join(results_path, f'TRASY/{file_name}'), sep=',', index=False)

            print(f'Saved {file_name} file.')
            break
        break

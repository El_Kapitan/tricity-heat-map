import numpy as np
import csv
import re
import os

def transform():
    """
    Transform data into .csv file with indexes of processed middle points
    :return: returns nothing
    """
    file_list = os.listdir("./TRASY/")
    ids =np.loadtxt('./middle_point_ids.csv', dtype=int)

    out_files = []

    for i in range(len(file_list)):
        id = re.search('\d+', file_list[i])
        sub = re.sub('\d+', str(np.argwhere(ids == int(id[0]))[0,0]), file_list[i], count=1)
        out_files.append(sub)


    for i in range(len(file_list)):
        # Read the csv file using the csv module
        with open(f"./TRASY/{file_list[i]}") as f:
            reader = csv.reader(f)
            # Skip the header
            next(reader)

            # Iterate over the rows in the csv file
            with open(f"./TRASYOUT/{out_files[i]}", 'w') as out:
                for row in reader:
                    # The route trace is the second element in each row
                    route_raw = row[1]
                    route_search = re.findall('\((\d+)', route_raw)
                    last = ''
                    if len(route_search) > 0:
                        route_search = [np.argwhere(ids == int(i))[0][0] for i in route_search]
                        last = f',{np.argwhere(ids == int(row[0]))[0][0]}'
                        
                    out.write(f'{str(route_search)[1:-1]}{last}\n')

if __name__ == '__main__':
    transform()
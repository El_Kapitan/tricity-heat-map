import os
import pandas as pd


def func():
    """
    Transforms data into .csv format
    :return: returns nothing
    """
    # directory containing the time.csv and data.csv files
    directory = "./CZASY"
    directory_out = "./CZASYOUT"
    data_df = pd.read_csv("./middle_point_id_to_coords.csv")

    # loop through the files in the directory
    for filename in os.listdir(directory):
        # skip files that are not .csv
        if not filename.endswith(".csv"):
            continue

        # read the file into a pandas dataframe
        df = pd.read_csv(os.path.join(directory, filename))

        # merge the two dataframes on the "middle_point_id" column
        df = df.merge(data_df, on="middle_point_id")

        # drop the first column from the dataframe
        # merged_df = merged_df.reset_index(drop=True)
        df = df.drop(df.columns[0], axis=1)

        # move the first column to the end of the dataframe
        df = df[[*df.columns[1:], df.columns[0]]]

        # write the resulting dataframe to a new file
        df.to_csv(os.path.join(directory_out, filename), index=False, header=False)


if __name__ == '__main__':
    func()
import folium
import json
import csv
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

"""
cal_mid_points.py
====================================
This script calculates points that are in the bounds of tricity.
"""


def calc_points():
    """
    Generates .csv file containing points that are within the borders of tricity and .html document
    for checking if the points are being calculated properly
    :return: returns nothing
    """
    # generating map object with proper zoom, focus on proper coordinates and clear color scheme
    tricity_map = folium.Map(location=[49.575600, 11.301994],
                             width='100%',
                             height='100%',
                             tiles='OpenStreetMap',
                             zoom_start=5,
                             zoom_control=True,
                             scrollWheelZoom=True)

    points = []
    europe = "europe.geojson" 
    united_kingdom = "united_kingdom.geojson"
    ireland = "ireland.geojson"
    lat_interval = 0.648 # 36 x 36 km
    lon_interval = 0.324 # distance between middle points latitude and
                         # longitude wise

    # coordinates of specific regions
    region_dict = {
        europe: (-12.834856, 31.691376, 35.176664, 72.835744),
        united_kingdom: (-6.89, 1.876, 49.62, 58.71),
        ireland: (-10.61, -4.51, 51.34, 55.41)
    }

    # main loop for computing and saving points
    for key in region_dict:
        points = open_geojson(key)
        corr = region_dict[key]
        mid_points = create_polygon(
            points, lat_interval, lon_interval, corr, tricity_map)
        if key == "europe.geojson":
            save_csv(mid_points, 'w')
        else:
            save_csv(mid_points, 'a')
    # adding red boundary read from .geojson file into map
        add_outline(key, tricity_map)

    # saving map as a .html document
    tricity_map.save("map.html")


def open_geojson(path):
    """
    Opens geojeson file for specific region and saves its border as
    points in single array
    :param path: path to specific geojson
    :type path: string
    :return: Returns borderline points of specified region 
    :rtype: list
    """
    points = []
    with open(f"./{path}", 'r') as f:
        data = json.loads(f.read())
        for i in (data["features"][0]["geometry"]["coordinates"]):
            points.append((i[0], i[1]))
    return points


def create_polygon(points, lat_interval, lon_interval, corr, tricity_map):
    """
    This function calculates points within specified region
    :param points: path to specific geojson file
    :type points: string
    :param lat_interval: value of distance between points latitude wise
    :type lat_interval: float
    :param lon_interval: value of distance between points longitude wise
    :type lon_interval: float
    :param corr: points which specifes region
    :type corr: tuple
    :param tricity_map: object which represents map
    :type tricity_map: folium map object
    :return: list of points within region
    :rtype: list
    """
    middle_point = []
    start_lat, end_lat, start_lon, end_lon = corr
    polygon = Polygon(points)
    for i in np.arange(start_lat, end_lat, lat_interval):
        for j in np.arange(start_lon, end_lon, lon_interval):
            point = Point(i, j)
            if polygon.contains(point):
                middle_point.append((j, i))
                folium.Circle(location=(j, i), radius=10, color="#3186cc").add_to(
                    tricity_map)
    return middle_point


def add_outline(region, map):
    """
    This function is drawing a border of specified region based on geojson
    file
    :param region: path to specific geojson
    :type region: string
    :param map: object which represents map
    :type map: folium map object
    :return: returns nothing
    """
    folium.Choropleth(geo_data=region, line_color='red', fill_opacity=0,
                      line_weight=3, line_opacity=0.7).add_to(map)


def save_csv(mid_points, write_mode):
    """
    This function saves middle points into single .csv file
    :param mid_points: list of points saved as coordinates
    :type mid_points: list
    :param write_mode: specifies in which mode .csv file should be opened 
    :type write_mode: string
    :return: returns nothing
    """
    with open("middle_points_eu.csv", write_mode, newline='') as csv_file:
        c_writer = csv.writer(csv_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        for point in mid_points:
            c_writer.writerow(point)


if __name__ == '__main__':
    calc_points()
import os
import numpy as np
import pandas as pd
from datetime import datetime, timedelta


main_path = '..\\gtfs_data'
gtfs_path = os.path.join(main_path, 'GTFS_PRZEWOZNIKOW')

my_carrier_folder = 'folder'
my_carrier_path = os.path.join(gtfs_path, my_carrier_folder)

# środa 16.11.2022 bez godzin
# wtorek 22.11.2022 godzina 23:59
BOTTOM_TIME_THRESH = datetime(2022, 11, 16, 0, 0, 0)
TOP_TIME_THRESH = datetime(2022, 11, 22, 23, 59, 59)

calendar = pd.read_csv(os.path.join(my_carrier_path, 'calendar.txt'), parse_dates=['start_date', 'end_date'], sep=',')

calendar_dates = pd.DataFrame(columns=['service_id', 'date', 'exception_type'])


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


weekdays = {0: 'monday',
            1: 'tuesday',
            2: 'wednesday',
            3: 'thursday',
            4: 'friday',
            5: 'saturday',
            6: 'sunday'}

for index, row in calendar.iterrows():
    start_date = row['start_date'].to_pydatetime()
    end_date = row['end_date'].to_pydatetime()

    print(f'\r{index}/{len(calendar)}', end='')

    # Dla service-ów w naszym porządanym przedziale tworzy rzeczywiste wpisy w calendar_dates
    # dla tych poza tworzy śmieciowe wpisy
    if start_date < TOP_TIME_THRESH and end_date > BOTTOM_TIME_THRESH:
        if start_date < BOTTOM_TIME_THRESH:
            start_date = BOTTOM_TIME_THRESH
        if end_date > TOP_TIME_THRESH:
            end_date = TOP_TIME_THRESH

        for single_date in daterange(start_date, end_date + timedelta(days=1)):
            if row[weekdays[single_date.weekday()]] == 1:
                calendar_dates.loc[len(calendar_dates.index)] = [row['service_id'], single_date.strftime('%Y%m%d'), '1']
    else:
        calendar_dates.loc[len(calendar_dates.index)] = [row['service_id'], start_date.strftime('%Y%m%d'), '1']

calendar_dates.to_csv(os.path.join(my_carrier_path, 'calendar_dates.txt'), index=False)
